package com.example.aritetrisburg;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;



public class ScoreLeaderboardScreen extends AppCompatActivity {
    Button toHomeButton;

    TextView first_place_points, second_place_points, third_place_points;
    //TextView tvHighest;
    SharedPreferences sharedPreferences;
    //ImageView ivNewHighest;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.score_leaderboard_screen);

        /////
//        first_place_points = findViewById(R.id.txtScore1);
//        second_place_points = findViewById(R.id.txtScore2);
//        third_place_points = findViewById(R.id.txtScore3);
//
//
//        int points = getIntent().getExtras().getInt("points");
//        second_place_points.setText("" + points);
//
//        sharedPreferences = getSharedPreferences("my_pref", 0);
//        int highest = sharedPreferences.getInt("highest", 0);
//        if (points > highest){
//            second_place_points.setVisibility(View.VISIBLE);
//            highest = points;
//            SharedPreferences.Editor editor = sharedPreferences.edit();
//            editor.putInt("highest", highest);
//            editor.commit();
//        }
//
//        second_place_points.setText("" + highest);

        ////

        toHomeButton = findViewById(R.id.btnLeaderboard);
        toHomeButton.setOnClickListener(v -> {
            Intent intent = new Intent(ScoreLeaderboardScreen.this, HomeScreen.class);
            startActivity(intent);

        });
    }
}